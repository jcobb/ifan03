If you are using the IFan03 module in the United States or on a 110V AC system, you will need to replace the capacitors in order to make all fan speeds avaiable.

https://www.ebay.com/itm/2pcs-BM-MKP-X2-5UF-400V-275VAC-275V-AC-for-Induction-cooker-etc/222885364374?pageci=5599f3b3-c1f4-41b4-b225-5fa6115edf5b&fbclid=IwAR0af3yLOVDLBg6GJbibTRWQde7DDtYW4T_P1xqdchDJH5YymiEt0Q2NDCw

https://www.aliexpress.com/item/32808846668.html?srcSns=sns_Copy&spreadType=socialShare&bizType=ProductDetail&tt=MG&fbclid=IwAR2rWRqN2NNabEnlQP0XPN0Hosgks12I9Q3kq-iZr82qv9EXDFHP7v-Nuec&aff_platform=default&sk=_m0g3w3f&mergeHashcode=60350961775&description=US+%249.65++8%EF%BC%85+Off+%7C+NEW+10pcs+Cooker+capacitance+275V-400V+5UF+high-voltage+capacitors&aff_trace_key=b78959b927314fe69957977c9126220f-1606672439514-07281-_m0g3w3f&businessType=ProductDetail&title=US+%249.65++8%EF%BC%85+Off+%7C+NEW+10pcs+Cooker+capacitance+275V-400V+5UF+high-voltage+capacitors&platform=AE&terminal_id=b12361ccbb2e4e22adfcb706e5ce56e7&templateKey=white_backgroup_101

Step 1. Copy the Ifan03.h file into your ESPHome folder in Home Assistant.
You can accomlish this using the File Editor plugin.

Step 2.  Copy the ifan03.yaml into the config editor in the HA ESPHome plugin for your Ifan03 entity.

Step 3.  Set up your Wifi SSID and password, and IP Addressing as necessary.  By default this config uses DHCP. 
		 Make sure this is correct before you upload, or you will have difficulty reconnecting to your device.
		
Step 4.  Change the names of the light and fan components as you see fit.

Step 5.  Upload

Boom done